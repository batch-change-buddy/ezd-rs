# `ezd` - eZ Docker Task Runner

[![Latest Release](https://gitlab.com/sbenv/veroxis/ezd-rs/-/badges/release.svg)](https://gitlab.com/sbenv/veroxis/ezd-rs/-/releases)

`ezd` is an easy to configure docker-based task runner system.

## Getting started

1. Create an `ezd.yml` file in your project root.
2. Customize the file. Examples:
   - Rust:
     - [ezd-rs](<https://gitlab.com/sbenv/veroxis/ezd-rs/-/blob/master/ezd.yml>)
     - [docker-seq](<https://gitlab.com/sbenv/veroxis/docker-seq/-/blob/master/ezd.yml>)
     - [jeeves](<https://gitlab.com/Veroxis/jeeves/-/blob/master/ezd.yml>)
3. Run your Tasks using `ezd run $TASK_NAME`
   - To get a short view of your tasks use `ezd list`
   - To inspect the final configuration use `ezd describe`
   - To inspect a specific task use `ezd describe $TASK_NAME`

## Documentation

### Project configuration

At first `ezd` will search for it's corresponding configuration file by searching the current path and traversing upwards until it finds an `ezd.yaml`, `ezd.json` or `ezd.yml` file.

The _project root_ is the directory in which `ezd.yaml`, `ezd.json` or `ezd.yml` is found. All tasks executed by `ezd` are relative to the _project root_.

### shell_exec

`shell_exec` tasks execute the given commands within a local shell.

### docker_seq

`ezd` provides integration for [docker-seq](<https://gitlab.com/sbenv/veroxis/docker-seq>).

### Variables and Constants

`ezd` provides a few constants:

- `EZD_HOST_PROJECT_ROOT`
- `EZD_HOST_USERNAME`
- `EZD_HOST_UID`
- `EZD_HOST_GID`

They can be used as __ENV__ variables within commands: `echo "my UID is ${EZD_HOST_UID}"`

They can be used as __constants__ aswell:

```yaml
tasks:
   shell_exec:
      whoami:
         commands:
            - echo "__EZD_HOST_USERNAME__"
```

__constants__ will be replaced by `ezd` internally before executing anything.

The last available __ENV__ variable is `$EZD_FORWARDED_ARGS`. This variable is populated with the values after `--` when executing `ezd run`. For example `ezd run SOME_TASK -- ls -la` would set `$EZD_FORWARDED_ARGS` to `ls -la` within the task `SOME_TASK`.

### docker volumes

`ezd` supports creating docker volumes:

```yaml
volumes:
   my_volume:
```

## Installation

### Binary Releases

Download pre-built binaries [here](<https://gitlab.com/sbenv/veroxis/ezd-rs/-/releases>).

### From Source

#### Prerequisites

- [Install Rust](<https://www.rust-lang.org/tools/install>)

#### Building

```sh
# clone the repo
git clone https://gitlab.com/sbenv/veroxis/ezd-rs.git && cd ezd-rs

# build your binary
cargo build --release

# there it is
target/release/ezd --help
```

## License

`ezd` is released under the Apache 2.0 license. See [LICENSE.txt](LICENSE.txt)
