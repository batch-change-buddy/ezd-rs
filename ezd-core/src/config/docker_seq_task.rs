use docker_seq_lib::docker_seq::DockerSeq;
use docker_seq_lib::docker_seq::DockerSeqError;
use serde::Deserialize;
use serde::Serialize;
use thiserror::Error;

use super::ezd::find_config_path;
use super::ezd::FindConfigPathError;

#[derive(Debug, Error)]
pub enum DockerSeqTaskError {
    #[error("IoError: `{0}`")]
    IoError(#[from] std::io::Error),

    #[error("DockerSeqError: `{0}`")]
    DockerSeqError(#[from] DockerSeqError),

    #[error("FindConfigPathError: `{0}`")]
    FindConfigPathError(#[from] FindConfigPathError),

    #[error("ChangeProjectDirError")]
    ChangeProjectDirError,
}

#[derive(Debug, Default, Serialize, Deserialize, Clone)]
#[serde(default)]
pub struct DockerSeqTask {
    pub description: String,
    pub config: DockerSeq,
}

impl DockerSeqTask {
    pub fn exec(&self) -> Result<(), DockerSeqTaskError> {
        let config_path = find_config_path()?;
        let config_dir = config_path
            .parent()
            .ok_or(DockerSeqTaskError::ChangeProjectDirError)?;
        std::env::set_current_dir(config_dir)?;
        let config_json = self.config.to_json()?;
        docker_seq_lib::run(config_json.as_str())?;
        Ok(())
    }
}
