## [1.10.1](https://gitlab.com/sbenv/veroxis/ezd-rs/compare/v1.10.0...v1.10.1) (2022-11-12)


### Bug Fixes

* use build-std to reduce binary size ([f525032](https://gitlab.com/sbenv/veroxis/ezd-rs/commit/f525032e9809c3a0681abb39fcb9fe5c317b4980))

# [1.10.0](https://gitlab.com/sbenv/veroxis/ezd-rs/compare/v1.9.3...v1.10.0) (2022-11-12)


### Bug Fixes

* remove unnecessary clap args ([a85d5e3](https://gitlab.com/sbenv/veroxis/ezd-rs/commit/a85d5e3a036bf0b1e4801378c6f58844f2e7fd5a))


### Features

* add workdir option ([4352fa0](https://gitlab.com/sbenv/veroxis/ezd-rs/commit/4352fa0d8aa6de66e7531a43851b1fd9a4281d45))

## [1.9.3](https://gitlab.com/sbenv/veroxis/ezd-rs/compare/v1.9.2...v1.9.3) (2022-09-28)


### Bug Fixes

* migrate clap 3.x to clap 4.x ([e52e3ed](https://gitlab.com/sbenv/veroxis/ezd-rs/commit/e52e3ed51d1e0649cffb5ce045c7712f782fcdf6))

## [1.9.2](https://gitlab.com/sbenv/veroxis/ezd-rs/compare/v1.9.1...v1.9.2) (2022-09-23)


### Bug Fixes

* create release profile for macos specifically ([d0aa610](https://gitlab.com/sbenv/veroxis/ezd-rs/commit/d0aa610c514b068ed6d79d77a8a48615297dcd19))

## [1.9.1](https://gitlab.com/sbenv/veroxis/ezd-rs/compare/v1.9.0...v1.9.1) (2022-09-13)


### Bug Fixes

* **jsonschema:** remove explicit jsonschema ([be83a6b](https://gitlab.com/sbenv/veroxis/ezd-rs/commit/be83a6bac0ec6fdc4db6c4738f85b636c0c037ee))

# [1.9.0](https://gitlab.com/sbenv/veroxis/ezd-rs/compare/v1.8.3...v1.9.0) (2022-08-29)


### Features

* **json:** enable using `ezd.json` ([a383e94](https://gitlab.com/sbenv/veroxis/ezd-rs/commit/a383e94a58a6948f4413e58a575d89b3512f725e))

## [1.8.3](https://gitlab.com/sbenv/veroxis/ezd-rs/compare/v1.8.2...v1.8.3) (2022-08-11)


### Bug Fixes

* **clippy:** use `.first()` ([7b74c9d](https://gitlab.com/sbenv/veroxis/ezd-rs/commit/7b74c9d34059095a9d07ba1934b7e273e087ffbb))

## [1.8.2](https://gitlab.com/sbenv/veroxis/ezd-rs/compare/v1.8.1...v1.8.2) (2022-07-19)


### Bug Fixes

* remove debugging code in CI ([b8ac8f1](https://gitlab.com/sbenv/veroxis/ezd-rs/commit/b8ac8f114ac02a32e0537c6356da7e9442199061))

## [1.8.1](https://gitlab.com/sbenv/veroxis/ezd-rs/compare/v1.8.0...v1.8.1) (2022-07-19)


### Bug Fixes

* add safeguard to fail if release artifact has not been created ([10b157d](https://gitlab.com/sbenv/veroxis/ezd-rs/commit/10b157d393869c0201aae070347e932b330a6f43))

# [1.8.0](https://gitlab.com/sbenv/veroxis/ezd-rs/compare/v1.7.2...v1.8.0) (2022-07-13)


### Features

* use alpine image from gitlab ([b23ae89](https://gitlab.com/sbenv/veroxis/ezd-rs/commit/b23ae89a8d262a5f8c0a0318a112cfc22a306a41))

## [1.7.2](https://gitlab.com/sbenv/veroxis/ezd-rs/compare/v1.7.1...v1.7.2) (2022-07-12)


### Bug Fixes

* **ci:** set correct artifact paths ([92a7a03](https://gitlab.com/sbenv/veroxis/ezd-rs/commit/92a7a03cac3fc25e3e05bedbad48c4b8c15e97d4))

## [1.7.1](https://gitlab.com/sbenv/veroxis/ezd-rs/compare/v1.7.0...v1.7.1) (2022-07-12)


### Bug Fixes

* **ci:** add artifacts for workspace Cargo.toml files ([0055e49](https://gitlab.com/sbenv/veroxis/ezd-rs/commit/0055e4974e95ae6a4b0b0065848a3b692024a164))

# [1.7.0](https://gitlab.com/sbenv/veroxis/ezd-rs/compare/v1.6.0...v1.7.0) (2022-07-12)


### Features

* add `init` command ([7d65453](https://gitlab.com/sbenv/veroxis/ezd-rs/commit/7d65453330c7c7cad5d8e36be7dbef63572d1509))
* add constant $EZD containing the path of the current ezd executable ([fdc7293](https://gitlab.com/sbenv/veroxis/ezd-rs/commit/fdc729386be56e83984fd370d95439f032b45742))
* allow `ezd.yaml` and `ezd.yml` as project file ([0efd45b](https://gitlab.com/sbenv/veroxis/ezd-rs/commit/0efd45b6f1da7dd34353f2582be95100b4a8df48))

# [1.6.0](https://gitlab.com/sbenv/veroxis/ezd-rs/compare/v1.5.1...v1.6.0) (2022-07-12)


### Features

* **docker-volume:** resolve `~` as `${HOME}` through updating `docker-seq` ([5c7fa33](https://gitlab.com/sbenv/veroxis/ezd-rs/commit/5c7fa33297f8a16c855a21e0aa1bbcca0192fca3))

## [1.5.1](https://gitlab.com/sbenv/veroxis/ezd-rs/compare/v1.5.0...v1.5.1) (2022-06-20)


### Bug Fixes

* update docs ([1b9565e](https://gitlab.com/sbenv/veroxis/ezd-rs/commit/1b9565e26e02a202a9df5e0aab19f941066f0433))

# [1.5.0](https://gitlab.com/sbenv/veroxis/ezd-rs/compare/v1.4.7...v1.5.0) (2022-06-20)


### Features

* add more distributed platforms ([c5d13b1](https://gitlab.com/sbenv/veroxis/ezd-rs/commit/c5d13b1d067a131b0ed6aff9bdddd5e7002fa938))

## [1.4.7](https://gitlab.com/sbenv/veroxis/ezd-rs/compare/v1.4.6...v1.4.7) (2022-06-15)


### Bug Fixes

* **deps:** update rust crate clap to 3.2.5 ([fd543d3](https://gitlab.com/sbenv/veroxis/ezd-rs/commit/fd543d3ec581a765e4053ee6aa12ef45bd296491))

## [1.4.6](https://gitlab.com/sbenv/veroxis/ezd-rs/compare/v1.4.5...v1.4.6) (2022-06-14)


### Bug Fixes

* **deps:** update rust crate clap to 3.2.4 ([190e6ce](https://gitlab.com/sbenv/veroxis/ezd-rs/commit/190e6cea4359a126d7d89768a64da1be37d3e525))

## [1.4.5](https://gitlab.com/sbenv/veroxis/ezd-rs/compare/v1.4.4...v1.4.5) (2022-06-14)


### Bug Fixes

* **deps:** update rust crate clap to 3.2.3 ([d1fdbfe](https://gitlab.com/sbenv/veroxis/ezd-rs/commit/d1fdbfed9d3f1a7c9ed284a074a6759cff6a667c))

## [1.4.4](https://gitlab.com/sbenv/veroxis/ezd-rs/compare/v1.4.3...v1.4.4) (2022-06-14)


### Bug Fixes

* **deps:** update rust crate clap to 3.2.2 ([da4452a](https://gitlab.com/sbenv/veroxis/ezd-rs/commit/da4452a29f10c97c332bfb44bac660e064341bc5))

## [1.4.3](https://gitlab.com/sbenv/veroxis/ezd-rs/compare/v1.4.2...v1.4.3) (2022-06-13)


### Bug Fixes

* **deps:** update rust crate clap to 3.2.1 ([c71e8a0](https://gitlab.com/sbenv/veroxis/ezd-rs/commit/c71e8a0c22df2891d8ffa44a9f92911b0d3704d8))
* use clap's new value_parser attribute ([b00474e](https://gitlab.com/sbenv/veroxis/ezd-rs/commit/b00474e9208d8111936a39c61ce876b84043b564))

## [1.4.2](https://gitlab.com/sbenv/veroxis/ezd-rs/compare/v1.4.1...v1.4.2) (2022-06-09)


### Bug Fixes

* **deps:** update rust repo https://gitlab.com/sbenv/veroxis/docker-seq to v1.4.3 ([8af6734](https://gitlab.com/sbenv/veroxis/ezd-rs/commit/8af67349e9d221a2bad54643989142eff302d666))

## [1.4.1](https://gitlab.com/sbenv/veroxis/ezd-rs/compare/v1.4.0...v1.4.1) (2022-05-28)


### Bug Fixes

* **version:** update `docker-seq` ([c117b3d](https://gitlab.com/sbenv/veroxis/ezd-rs/commit/c117b3d340b378881cc4d69535570109f0c28a68))

# [1.4.0](https://gitlab.com/sbenv/veroxis/ezd-rs/compare/v1.3.0...v1.4.0) (2022-05-28)


### Features

* add json highlighting ([b08bad4](https://gitlab.com/sbenv/veroxis/ezd-rs/commit/b08bad4efbac447c549c828caf36ef9524e6bfc0))

# [1.3.0](https://gitlab.com/sbenv/veroxis/ezd-rs/compare/v1.2.0...v1.3.0) (2022-05-27)


### Features

* remove syntect highlighting ([c829aaf](https://gitlab.com/sbenv/veroxis/ezd-rs/commit/c829aaf4b9f6404c725d5dbd124a25447b4d3f78))

# [1.2.0](https://gitlab.com/sbenv/veroxis/ezd-rs/compare/v1.1.0...v1.2.0) (2022-05-26)


### Bug Fixes

* **clippy:** use `.nth(2)` instead of `.skip(1).next()` ([433873f](https://gitlab.com/sbenv/veroxis/ezd-rs/commit/433873fdaeaacc8ca3e89cb74b863c320a0f9439))


### Features

* **run:** implement docker volume handling ([71951a4](https://gitlab.com/sbenv/veroxis/ezd-rs/commit/71951a44bb821b3c6e7e7fc0631ede188fd388b8))

# [1.1.0](https://gitlab.com/sbenv/veroxis/ezd-rs/compare/v1.0.3...v1.1.0) (2022-05-26)


### Features

* **release:** add a aarch64-unknown-linux-musl release ([c2d5122](https://gitlab.com/sbenv/veroxis/ezd-rs/commit/c2d51229c7245f8974b3f9646bef55dc10de8fa9))

## [1.0.3](https://gitlab.com/sbenv/veroxis/ezd-rs/compare/v1.0.2...v1.0.3) (2022-05-23)


### Bug Fixes

* improved error handling ([9a6bc8f](https://gitlab.com/sbenv/veroxis/ezd-rs/commit/9a6bc8fdafe6c150100a07cfa15102d9f0afaf7b))

## [1.0.2](https://gitlab.com/sbenv/veroxis/ezd-rs/compare/v1.0.1...v1.0.2) (2022-05-23)


### Bug Fixes

* set correct workdir when executing tasks ([7663e42](https://gitlab.com/sbenv/veroxis/ezd-rs/commit/7663e42ceab2d691e1afd860835366fdde4da94d))

## [1.0.1](https://gitlab.com/sbenv/veroxis/ezd-rs/compare/v1.0.0...v1.0.1) (2022-05-23)


### Bug Fixes

* **ci:** automatically update `Cargo.lock` in releases ([a420bf8](https://gitlab.com/sbenv/veroxis/ezd-rs/commit/a420bf88e5f0018d6855cda9ed5009c4bb69ee1a))

# 1.0.0 (2022-05-23)


### Bug Fixes

* reduced stack memory usage by using a pointer ([38650ed](https://gitlab.com/sbenv/veroxis/ezd-rs/commit/38650ed709dff35ea9aba2de7a672e51d08192fb))


### Features

* added `did you mean any of these?` message when asking for non-existing task names ([7a82d3a](https://gitlab.com/sbenv/veroxis/ezd-rs/commit/7a82d3a1565163ef85ae3d7ca6c876d0219b9d01))
* create working initial version ([36d5127](https://gitlab.com/sbenv/veroxis/ezd-rs/commit/36d5127c343e8a5707d40898048a7b928239f84e))
* disable colored text when not in a tty when using `describe` ([a2d1ed5](https://gitlab.com/sbenv/veroxis/ezd-rs/commit/a2d1ed546d8c0aad57a3cd980f8997e86fb4338b))
* **release:** add semantic-release ([6d55b56](https://gitlab.com/sbenv/veroxis/ezd-rs/commit/6d55b56ecac594fa11bd18726b93e9e1eb305ee2))
* switch to `atty` for tty detection instead of using raw `libc` ([196c25c](https://gitlab.com/sbenv/veroxis/ezd-rs/commit/196c25c3fbd3258c257048bd4655aac6099c8a26))

# Changelog
All notable changes to this project will be documented in this file. See [conventional commits](https://www.conventionalcommits.org/) for commit guidelines.

- - -
## 0.1.0 - 2022-04-06
#### Bug Fixes
- reduced stack memory usage by using a pointer - (38650ed) - Daniel Christian Kupczak
#### Continuous Integration
- **(gitlab)** added `.gitlab-ci.yml` - (34be86c) - Daniel Christian Kupczak
#### Documentation
- **(changelog)** added changelog - (591ebab) - Daniel Christian Kupczak
#### Features
- switch to `atty` for tty detection instead of using raw `libc` - (196c25c) - Daniel Christian Kupczak
- disable colored text when not in a tty when using `describe` - (a2d1ed5) - Daniel Christian Kupczak
- added `did you mean any of these?` message when asking for non-existing task names - (7a82d3a) - Daniel Christian Kupczak
#### Miscellaneous Chores
- **(changelog)** deleted changelog - (3206d93) - Daniel Christian Kupczak
- - -

Changelog generated by [cocogitto](https://github.com/cocogitto/cocogitto).
