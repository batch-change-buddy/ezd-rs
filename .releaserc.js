function get_assets() {
    const binary_name = 'ezd';
    const targets = get_targets();
    let assets = [];

    targets.forEach((target) => {
        assets.push({
            label: `${binary_name}-\${nextRelease.version}-${target}`,
            path: `target/${target}/release/${binary_name}`
        });
    });

    return assets;
}

function get_targets() {
    return [
        "x86_64-unknown-linux-musl",
        "aarch64-unknown-linux-musl",
        "arm-unknown-linux-musleabihf",
        "x86_64-apple-darwin",
        "aarch64-apple-darwin",
    ];
}

module.exports = {
    branches: ['*'],
    plugins: [
        "@semantic-release/commit-analyzer",
        "@semantic-release/release-notes-generator",
        "@semantic-release/changelog",
        ['@semantic-release/git', {
            assets: ['CHANGELOG.md', "Cargo.toml", "Cargo.lock", "ezd-cli/Cargo.toml", "ezd-core/Cargo.toml"],
        }],
        ['@semantic-release/gitlab', {
            assets: get_assets(),
        }]
    ],
};
