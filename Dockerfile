FROM registry.gitlab.com/sbenv/veroxis/images/alpine:3.16

COPY target/x86_64-unknown-linux-musl/release/ezd /ezd_x86_64-unknown-linux-musl
COPY target/aarch64-unknown-linux-musl/release/ezd /ezd_aarch64-unknown-linux-musl
COPY target/arm-unknown-linux-musleabihf/release/ezd /ezd_arm-unknown-linux-musleabihf
COPY target/x86_64-apple-darwin/release/ezd /ezd_x86_64-apple-darwin
COPY target/aarch64-apple-darwin/release/ezd /ezd_aarch64-apple-darwin
