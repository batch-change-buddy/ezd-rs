use std::io::stdout;
use std::io::Write;

use colored::Colorize;
use ezd_core::config::ezd::Ezd;

pub fn print_similar_tasks_help(ezd: &Ezd, task_name: &str) {
    let levenshtein_distance = 3;
    let prefix = format!("[{}]", "ezd".green());
    let mut msg = format!("Task `{}` does not exist.", task_name.purple());
    println!("{prefix} {}", msg.green());
    let all_tasks = ezd.get_tasks();
    let similar_tasks_levenshtein = all_tasks
        .iter()
        .filter(|available_task| {
            levenshtein::levenshtein(task_name, available_task) < levenshtein_distance
        })
        .collect::<Vec<_>>();
    let similar_tasks_substring = all_tasks
        .iter()
        .filter(|available_task| available_task.contains(task_name))
        .collect::<Vec<_>>();
    let mut all_similar_tasks = vec![];
    all_similar_tasks.extend(similar_tasks_levenshtein);
    all_similar_tasks.extend(similar_tasks_substring);
    all_similar_tasks.sort();
    all_similar_tasks.dedup();
    if all_similar_tasks.is_empty() {
        msg = "No similar tasks exist.".to_string();
        println!("{prefix} {}", msg.green());
    } else {
        msg = "Did you mean any of these?".to_string();
        println!("{prefix} {}", msg.green());
        for task in all_similar_tasks {
            msg = format!("`{}`", task.purple());
            println!("{prefix}     {}", msg.green());
        }
    }
}

pub trait PrettyPrint {
    type Error;

    fn pretty_print(&self) -> Result<(), Self::Error>;
}

impl PrettyPrint for serde_json::Value {
    type Error = serde_json::Error;

    fn pretty_print(&self) -> Result<(), Self::Error> {
        let mut lock = stdout().lock();
        colored_json::write_colored_json(self, &mut lock)?;
        let _ = lock.write(&[b'\n']).expect("failed to write to stdout");
        lock.flush().expect("failed to flush stdout");
        Ok(())
    }
}
